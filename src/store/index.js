import { createStore } from 'vuex'
import userModule from './modules/user'
import countryModule from './modules/country'

const store = createStore({
  modules: {
    user: userModule,
    country: countryModule
  }
})

export default store
