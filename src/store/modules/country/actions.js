import {
  FETCH_COUNTRIES,
  SET_COUNTRIES_DATA,
} from "./types";
import axios from "axios";

export default {
  [FETCH_COUNTRIES](context) {

    if (!context.state.countries.length) {
      axios.get(process.env.VUE_APP_COUNTRY_LIST_URL).then((response) => {
        const countriesData = response.data.map(({ name }) => name)

        context.commit(SET_COUNTRIES_DATA, countriesData)
      });
    }
  },
}
