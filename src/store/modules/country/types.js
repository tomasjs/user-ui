//actions 
export const FETCH_COUNTRIES = 'FETCH_COUNTRIES'

//mutations
export const SET_COUNTRIES_DATA = 'SET_COUNTRIES_DATA'

//getters
export const GET_COUNTRIES_DATA = 'GET_COUNTRIES_DATA'
