import { GET_COUNTRIES_DATA } from "./types";

export default {
  [GET_COUNTRIES_DATA]: (state) => {
    return state.countries
  }
}
