import { SET_COUNTRIES_DATA } from "./types";

export default {
  [SET_COUNTRIES_DATA]: (state, countriesData) => {
    state.countries = countriesData
  }
}
