import { SET_CREATE_USER_DATA, SET_UPDATE_USER_DATA, REMOVE_USER_DATA } from "./types";

export default {
  [SET_CREATE_USER_DATA]: (state, userData) => {
    state.push(userData)
  },
  [SET_UPDATE_USER_DATA]: (state, { id, userData }) => {
    state[id] = userData
  },
  [REMOVE_USER_DATA]: (state, id) => {
    state.splice(id, 1)
  }
}
