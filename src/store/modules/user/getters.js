import { GET_USER_DATA, GET_USER_LIST } from "./types";

export default {
  [GET_USER_LIST]: (state) => {
    return state
  },
  [GET_USER_DATA]: (state) => (id) => {
    return state[id]
  },
}
