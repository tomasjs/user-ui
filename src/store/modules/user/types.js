//actions 
export const CREATE_USER = 'CREATE_USER'
export const UPDATE_USER = 'UPDATE_USER'
export const DELETE_USER = 'DELETE_USER'

//mutations
export const SET_CREATE_USER_DATA = 'SET_CREATE_USER_DATA'
export const SET_UPDATE_USER_DATA = 'SET_UPDATE_USER_DATA'
export const REMOVE_USER_DATA = 'REMOVE_USER_DATA'

//getters
export const GET_USER_DATA = 'GET_USER_DATA'
export const GET_USER_LIST = 'GET_USER_LIST'
