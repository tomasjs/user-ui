import {
  CREATE_USER,
  UPDATE_USER,
  DELETE_USER,
  SET_CREATE_USER_DATA,
  SET_UPDATE_USER_DATA,
  REMOVE_USER_DATA
} from "./types";

export default {
  [CREATE_USER](context, userData) {
    context.commit(SET_CREATE_USER_DATA, userData)
  },
  [UPDATE_USER](context, { id, userData }) {
    context.commit(SET_UPDATE_USER_DATA, { id, userData })
  },
  [DELETE_USER](context, id) {
    context.commit(REMOVE_USER_DATA, id)
  },
}
