import { createRouter, createWebHistory } from 'vue-router';
import UserCreate from '../views/user/Create.vue';
import UserEdit from '../views/user/Edit.vue';
import UserList from '../views/user/List.vue';

const routes = [
  { path: '/', name: 'userCreate', component: UserCreate },
  { path: '/edit/:id', name: 'userEdit', component: UserEdit },
  { path: '/list', name: 'userList', component: UserList },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
